# frozen_string_literal: true

require 'yaml'
require_relative './json_base'

module Apigen
  module Formats
    module OpenAPI
      ##
      # OpenAPI 3 generator.
      module V3 # rubocop:disable Metrics/ModuleLength
        class << self # rubocop:disable Metrics/ClassLength
          include Apigen::Formats::JsonBase

          def generate(api) # rubocop:disable Metrics/AbcSize
            # TODO: Allow overriding any of the hardcoded elements.
            hash = { 'openapi' => '3.0.2' }
            hash.merge! 'info' => info(api) if info(api)
            hash.merge! 'servers' => servers(api.servers) if servers(api.servers)
            hash.merge! 'tags' => tags(api.tags) if tags(api.tags)
            hash.merge! 'paths' => paths(api)
            hash.merge! 'components' => { 'schemas' => definitions(api) } if definitions(api)
            hash['components'].merge! 'securitySchemes' => security_definitions(api) if security_definitions(api)
            hash.to_yaml
          end

          private

          def info(api) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity
            hash = {}
            hash.merge! 'version' => api.version if api.version
            hash.merge! 'title' => api.title if api.title
            hash.merge! 'description' => api.description if api.description
            hash.merge! 'termsOfService' => api.terms_of_service if api.terms_of_service
            hash.merge! 'contact' => contact(api.contact) if contact(api.contact)
            hash.merge! 'license' => license(api.license) if license(api.license)
            hash.empty? ? nil : hash
          end

          def contact(con)
            hash = {}
            hash.merge! 'name' => con.name if con.name
            hash.merge! 'email' => con.email if con.email
            hash.merge! 'url' => con.url if con.url
            hash.empty? ? nil : hash
          end

          def license(lic)
            hash = {}
            hash.merge! 'name' => lic.name if lic.name
            hash.merge! 'url' => lic.url if lic.url
            hash.empty? ? nil : hash
          end

          def servers(ser)
            array = []
            ser.each do |e|
              hash = {}
              hash.merge! 'url' => e.url if e.url
              hash.merge! 'description' => e.description if e.description
              array << hash unless hash.empty?
            end
            array.empty? ? nil : array
          end

          def tags(tgs)
            array = []
            tgs.each do |e|
              hash = {}
              hash.merge! 'name' => e.name if e.name
              hash.merge! 'description' => e.description if e.description
              array << hash unless hash.empty?
            end
            array.empty? ? nil : array
          end

          def paths(api) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity
            hash = {}
            api.endpoints.each do |endpoint|
              parameters = []
              parameters.concat(endpoint.path_parameters.properties.map do |name, property|
                                  path_parameter(api, name, property)
                                end)
              parameters.concat(endpoint.query_parameters.properties.map do |name, property|
                                  query_parameter(api, name, property)
                                end)
              parameters.concat(endpoint.header_parameters.properties.map do |name, property|
                                  header_parameter(api, name, property)
                                end)
              responses = endpoint.outputs.map { |output| response(api, output) }.to_h
              operation = {
                'operationId' => endpoint.name.to_s,
                'parameters' => parameters,
                'responses' => responses
              }
              add_description(operation, endpoint.description)
              operation['requestBody'] = input(api, endpoint.input) if endpoint.input
              operation['tags'] = endpoint.tags.map(&:name) if endpoint.tags
              operation['security'] = [{ endpoint.security => [] }] if endpoint.security
              hash[endpoint.path] ||= {}
              hash[endpoint.path][endpoint.method.to_s] = operation
            end
            hash
          end

          def path_parameter(api, name, property)
            parameter = {
              'in' => 'path',
              'name' => name.to_s,
              'required' => true,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def query_parameter(api, name, property)
            parameter = {
              'in' => 'query',
              'name' => name.to_s,
              'required' => property.required?,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def header_parameter(api, name, property)
            parameter = {
              'in' => 'header',
              'name' => name.to_s,
              'required' => property.required?,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def input(api, property) # rubocop:disable Metrics/MethodLength
            parameter = {
              'required' => true,
              'content' => {
                'application/json' => {
                  'schema' => schema(api, property.type)
                }
              }
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def response(api, output) # rubocop:disable Metrics/MethodLength
            response = {}
            add_description(response, output.description)
            add_example(response, output.example)
            if output.type != Apigen::PrimaryType.new(:void)
              response['content'] = {
                'application/json' => {
                  'schema' => schema(api, output.type)
                }
              }
            end
            [output.status.to_s, response]
          end

          def model_ref(type)
            "#/components/schemas/#{type}"
          end

          def supports_discriminator?
            true
          end
        end
      end
    end
  end
end
