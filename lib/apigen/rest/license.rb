# frozen_string_literal: true

module Apigen
  module Rest
    # License
    class License
      attribute_setter_getter :name
      attribute_setter_getter :url
      def initialize
        @name = nil
        @url = nil
      end
    end
  end
end
