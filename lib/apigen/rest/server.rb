# frozen_string_literal: true

module Apigen
  module Rest
    # Server
    class Server
      attribute_setter_getter :url
      attribute_setter_getter :description
      def initialize(url = nil, description = nil)
        @url = url
        @description = description
      end
    end
  end
end
