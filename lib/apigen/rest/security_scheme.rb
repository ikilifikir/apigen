# frozen_string_literal: true

module Apigen
  module Rest
    # SecurityScheme
    class SecurityScheme
      attribute_setter_getter :type
      attribute_setter_getter :name
      attribute_setter_getter :scheme
      attribute_setter_getter :description
      attribute_setter_getter :bearer_format
      attribute_setter_getter :apikey_name
      attribute_setter_getter :apikey_in
      def initialize(type, name, scheme = nil, description = nil, bearer_format = 'JWT', apikey_name = nil, apikey_in = nil) # rubocop:disable Metrics/ParameterLists, Layout/LineLength
        @type = type
        @name = name
        @scheme = scheme
        @description = description
        @bearer_format = bearer_format
        @apikey_name = apikey_name
        @apikey_in = apikey_in
      end

      def security_scheme
        value = {}
        if type == 'http' && scheme == 'bearer'
          value = { name => { 'type' => type, 'scheme' => scheme, 'bearerFormat' => bearer_format } }
        end
        value = { name => { 'type' => type, 'name' => apikey_name, 'in' => apikey_in } } if type == 'apiKey'
        value
      end
    end
  end
end
